When you do this, you get a new menu item (with access control restrictions) that leads to a page where you can adjust the parent-tree structure of
your book pages.

The JS for this page is under development and you should back-up your database before using this experimental module.