/*
*	Copyright David Wees January 12, 2007
*	You may use this script in anyway you see fit so long as you keep all of the copyright notices intact.
*	http://www.unitorganizer.com/myblog
*/

var oldPosition = "";

function findPosition(el, ex, ey) {
	var listItems = document.getElementById("bookwrap").getElementsByTagName("li");
	var lx, ly, lw, lh, er;
	var IEx = 0;
	if (window.createPopup) {
		if (document.getElementById("sidebar-left")) {
			IEx = xWidth(document.getElementById("sidebar-left"));
		}
	}
	var sy = xScrollTop(); //window, true
	var ul = document.getElementsByTagName("ul");
	for (var i = ul.length - 1; i >= 0; i--) {
		if (!ul[i].firstChild) {
			ul[i].parentNode.removeChild(ul[i]);
		}
	}	
	for (var i = listItems.length-1; i >= 0; i--) {
		if (/item/.test(listItems[i].id)) {
			er = Drupal.absolutePosition(listItems[i]);
			lx = er.x;  //xPageX(listItems[i]);
			ly = er.y;  //xPageY(listItems[i]);
			lh = xHeight(listItems[i]);
			lw = xWidth(listItems[i])/2;
			if ((ex >= (lx + lw - IEx)) && (ey >= (ly - sy )) && (ex <= (lx + 2*lw - IEx)) && (ey <= (ly - sy + lh))) {	
				var parent = el.parentNode;
				el = parent.removeChild(el);
				try {
					if (/\<li/.test(listItems[i].innerHTML)) {
						var listChildren = listItems[i].getElementsByTagName("li");
						listChildren[0].parentNode.insertBefore(el, listChildren[0]);
					} else {
						var ul = document.createElement("ul");
						ul.style.className = "boxy sortable";
						ul.style.width = "20em";
						ul.style.listStyle = "none";
						ul.appendChild(el);
						listItems[i].appendChild(ul);
					}
				}
				catch (e) {
					parent.appendChild(el);
				}
				return true;
			}
			if (((ex >= (lx - IEx)) && (ey >= (ly - sy))) && ((ex <= (lx + lw - IEx)) && (ey <= (ly - sy + lh)))) {
				var parent = el.parentNode;
				el = parent.removeChild(el);
				try {
					listItems[i].parentNode.insertBefore(el, listItems[i]);
				}
				catch (e) {
					parent.appendChild(el);
				}
				return true;
			}
		}
	}
	return false;
}

function updateParents() {
	var li = document.getElementsByTagName("li");
	var bookParents = "struct=";
	for (var i = li.length - 1; i >= 0; i--) {
		if (/item/.test(li[i].id)) {
			if (li[i].parentNode.parentNode.id === "bookwrap") {
				bookParents += "0;" + li[i].id.replace("item", "");
				bookParents += ":";
			} else {
				if (/item/.test(li[i].parentNode.parentNode.id)) {
					bookParents += li[i].parentNode.parentNode.id.replace("item", "") + ";" + li[i].id.replace("item", "");
					bookParents += ":";
				}
			}
		}
	}
	$.ajax({ type: "POST", url: "gui", data: bookParents, success: displayMessage});
	return;
}

function displayMessage(text) {
	if (text) {
		var notices = document.getElementById("notices");
		if (text == "true") {
			text = "Database updated!";
		} else {
			text = "Update of database failed.";
		}		
		notices.innerHTML = text;
		$(notices).fadeIn(1000, function() { $(document.getElementById("notices")).fadeOut(1000); });
	} else {
		var notices = document.getElementById("notices");
		notices.innerHTML = "update failed!";
		$(notices).fadeIn(1000, function() { $(document.getElementById("notices")).fadeOut(1000); });
	}
	document.getElementById("bookFormButton").disabled = false;
	oldPosition = document.getElementById("bookwrap").innerHTML;
}

function undoAllMoves() {
	document.getElementById("bookwrap").innerHTML = oldPosition;
	var li = document.getElementById("bookwrap").getElementsByTagName("li");
	for (var i = 0; i < li.length; i++) {
		if (/sortable/.test(li[i].parentNode.className)) {
			var span = li[i].getElementsByTagName("span")[0];
			Drag.init(span, li[i]);
		}
	}	
}

// xPageX, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL

function xPageX(e)
{
  if (!(e=xGetElementById(e))) return 0;
  var x = 0;
  while (e) {
    if (xDef(e.offsetLeft)) x += e.offsetLeft;
    e = xDef(e.offsetParent) ? e.offsetParent : null;
  }
  return x;
}

// xPageY, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL

function xPageY(el)
{
  if (!(el=xGetElementById(el))) return 0;
  var y = 0;
  while (el) {
    if (xDef(el.offsetTop)) y += el.offsetTop;
    el = xDef(el.offsetParent) ? el.offsetParent : null;
  }
  return y;
}

// xGetElementById, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL

function xGetElementById(el)
{
  if(typeof(el)=='string') {
    if(document.getElementById) e=document.getElementById(el);
    else if(document.all) el=document.all[el];
    else el=null;
  }
  return el;
}

// xDef, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL

function xDef()
{
  for(var i=0; i<arguments.length; ++i){if(typeof(arguments[i])=='undefined') return false;}
  return true;
}

// xHeight, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL

function xHeight(e,h)
{
  if(!(e=xGetElementById(e))) return 0;
  if (xNum(h)) {
    if (h<0) h = 0;
    else h=Math.round(h);
  }
  else h=-1;
  var css=xDef(e.style);
  if (e == document || e.tagName.toLowerCase() == 'html' || e.tagName.toLowerCase() == 'body') {
    h = xClientHeight();
  }
  else if(css && xDef(e.offsetHeight) && xStr(e.style.height)) {
    if(h>=0) {
      var pt=0,pb=0,bt=0,bb=0;
      if (document.compatMode=='CSS1Compat') {
        var gcs = xGetComputedStyle;
        pt=gcs(e,'padding-top',1);
        if (pt !== null) {
          pb=gcs(e,'padding-bottom',1);
          bt=gcs(e,'border-top-width',1);
          bb=gcs(e,'border-bottom-width',1);
        }
        // Should we try this as a last resort?
        // At this point getComputedStyle and currentStyle do not exist.
        else if(xDef(e.offsetHeight,e.style.height)){
          e.style.height=h+'px';
          pt=e.offsetHeight-h;
        }
      }
      h-=(pt+pb+bt+bb);
      if(isNaN(h)||h<0) return;
      else e.style.height=h+'px';
    }
    h=e.offsetHeight;
  }
  else if(css && xDef(e.style.pixelHeight)) {
    if(h>=0) e.style.pixelHeight=h;
    h=e.style.pixelHeight;
  }
  return h;
}

// xNum, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL

function xNum()
{
  for(var i=0; i<arguments.length; ++i){if(isNaN(arguments[i]) || typeof(arguments[i])!='number') return false;}
  return true;
}

// xStr, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL

function xStr(s)
{
  for(var i=0; i<arguments.length; ++i){if(typeof(arguments[i])!='string') return false;}
  return true;
}

// xGetComputedStyle, Copyright 2002-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL

function xGetComputedStyle(oEle, sProp, bInt)
{
  var s, p = 'undefined';
  var dv = document.defaultView;
  if(dv && dv.getComputedStyle){
    s = dv.getComputedStyle(oEle,'');
    if (s) p = s.getPropertyValue(sProp);
  }
  else if(oEle.currentStyle) {
    // convert css property name to object property name for IE
    var i, c, a = sProp.split('-');
    sProp = a[0];
    for (i=1; i<a.length; ++i) {
      c = a[i].charAt(0);
      sProp += a[i].replace(c, c.toUpperCase());
    }
    p = oEle.currentStyle[sProp];
  }
  else return null;
  return bInt ? (parseInt(p) || 0) : p;
}

// xWidth, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL

function xWidth(e,w)
{
  if(!(e=xGetElementById(e))) return 0;
  if (xNum(w)) {
    if (w<0) w = 0;
    else w=Math.round(w);
  }
  else w=-1;
  var css=xDef(e.style);
  if (e == document || e.tagName.toLowerCase() == 'html' || e.tagName.toLowerCase() == 'body') {
    w = xClientWidth();
  }
  else if(css && xDef(e.offsetWidth) && xStr(e.style.width)) {
    if(w>=0) {
      var pl=0,pr=0,bl=0,br=0;
      if (document.compatMode=='CSS1Compat') {
        var gcs = xGetComputedStyle;
        pl=gcs(e,'padding-left',1);
        if (pl !== null) {
          pr=gcs(e,'padding-right',1);
          bl=gcs(e,'border-left-width',1);
          br=gcs(e,'border-right-width',1);
        }
        // Should we try this as a last resort?
        // At this point getComputedStyle and currentStyle do not exist.
        else if(xDef(e.offsetWidth,e.style.width)){
          e.style.width=w+'px';
          pl=e.offsetWidth-w;
        }
      }
      w-=(pl+pr+bl+br);
      if(isNaN(w)||w<0) return;
      else e.style.width=w+'px';
    }
    w=e.offsetWidth;
  }
  else if(css && xDef(e.style.pixelWidth)) {
    if(w>=0) e.style.pixelWidth=w;
    w=e.style.pixelWidth;
  }
  return w;
}

// xScrollTop, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL

function xScrollTop(e, bWin)
{
  var offset=0;
  if (!xDef(e) || bWin || e == document || e.tagName.toLowerCase() == 'html' || e.tagName.toLowerCase() == 'body') {
    var w = window;
    if (bWin && e) w = e;
    if(w.document.documentElement && w.document.documentElement.scrollTop) offset=w.document.documentElement.scrollTop;
    else if(w.document.body && xDef(w.document.body.scrollTop)) offset=w.document.body.scrollTop;
  }
  else {
    e = xGetElementById(e);
    if (e && xNum(e.scrollTop)) offset = e.scrollTop;
  }
  return offset;
}

/**************************************************
 * dom-drag.js
 * 09.25.2001
 * www.youngpup.net
 **************************************************
 * 10.28.2001 - fixed minor bug where events
 * sometimes fired off the handle, not the root.
 **************************************************/

var Drag = {

    obj : null,

    init : function(o, oRoot, minX, maxX, minY, maxY, bSwapHorzRef, bSwapVertRef, fXMapper, fYMapper)
    {
        o.onmousedown    = Drag.start;

        o.hmode            = bSwapHorzRef ? false : true ;
        o.vmode            = bSwapVertRef ? false : true ;

        o.root = oRoot && oRoot != null ? oRoot : o ;

        if (o.hmode  && isNaN(parseInt(o.root.style.left  ))) o.root.style.left   = "0px";
        if (o.vmode  && isNaN(parseInt(o.root.style.top   ))) o.root.style.top    = "0px";
        if (!o.hmode && isNaN(parseInt(o.root.style.right ))) o.root.style.right  = "0px";
        if (!o.vmode && isNaN(parseInt(o.root.style.bottom))) o.root.style.bottom = "0px";

        o.minX    = typeof minX != 'undefined' ? minX : null;
        o.minY    = typeof minY != 'undefined' ? minY : null;
        o.maxX    = typeof maxX != 'undefined' ? maxX : null;
        o.maxY    = typeof maxY != 'undefined' ? maxY : null;

        o.xMapper = fXMapper ? fXMapper : null;
        o.yMapper = fYMapper ? fYMapper : null;

        o.root.onDragStart    = new Function();
        o.root.onDragEnd    = new Function();
        o.root.onDrag        = new Function();
    },

    start : function(e)
    {
        var o = Drag.obj = this;
        e = Drag.fixE(e);
        var y = parseInt(o.vmode ? o.root.style.top  : o.root.style.bottom);
        var x = parseInt(o.hmode ? o.root.style.left : o.root.style.right );
		
		o.root.onDragStart(x, y);

        o.lastMouseX    = e.clientX;
        o.lastMouseY    = e.clientY;

        if (o.hmode) {
            if (o.minX != null)    o.minMouseX    = e.clientX - x + o.minX;
            if (o.maxX != null)    o.maxMouseX    = o.minMouseX + o.maxX - o.minX;
        } else {
            if (o.minX != null) o.maxMouseX = -o.minX + e.clientX + x;
            if (o.maxX != null) o.minMouseX = -o.maxX + e.clientX + x;
        }

        if (o.vmode) {
            if (o.minY != null)    o.minMouseY    = e.clientY - y + o.minY;
            if (o.maxY != null)    o.maxMouseY    = o.minMouseY + o.maxY - o.minY;
        } else {
            if (o.minY != null) o.maxMouseY = -o.minY + e.clientY + y;
            if (o.maxY != null) o.minMouseY = -o.maxY + e.clientY + y;
        }

        document.onmousemove    = Drag.drag;
        document.onmouseup      = Drag.end;

        return false;
    },

    drag : function(e)
    {
        e = Drag.fixE(e);
        var o = Drag.obj;

        var ey    = e.clientY;
        var ex    = e.clientX;
        var y = parseInt(o.vmode ? o.root.style.top  : o.root.style.bottom);
        var x = parseInt(o.hmode ? o.root.style.left : o.root.style.right );
        var nx, ny;

        if (o.minX != null) ex = o.hmode ? Math.max(ex, o.minMouseX) : Math.min(ex, o.maxMouseX);
        if (o.maxX != null) ex = o.hmode ? Math.min(ex, o.maxMouseX) : Math.max(ex, o.minMouseX);
        if (o.minY != null) ey = o.vmode ? Math.max(ey, o.minMouseY) : Math.min(ey, o.maxMouseY);
        if (o.maxY != null) ey = o.vmode ? Math.min(ey, o.maxMouseY) : Math.max(ey, o.minMouseY);

        nx = x + ((ex - o.lastMouseX) * (o.hmode ? 1 : -1));
        ny = y + ((ey - o.lastMouseY) * (o.vmode ? 1 : -1));

        if (o.xMapper)    	  nx = o.xMapper(y);
        else if (o.yMapper)   ny = o.yMapper(x);

        Drag.obj.lastMouseX    = ex;
        Drag.obj.lastMouseY    = ey;

        Drag.obj.root.onDrag(nx, ny);
		
		findPosition(Drag.obj.root, ex, ey);
		
        return false;
    },

    end : function()
    {
        document.onmousemove = null;
        document.onmouseup   = null;
		
        Drag.obj.root.onDragEnd(    parseInt(Drag.obj.root.style[Drag.obj.hmode ? "left" : "right"]), 
                                    	parseInt(Drag.obj.root.style[Drag.obj.vmode ? "top" : "bottom"]));
        Drag.obj = null;

    },

    fixE : function(e)
    {
        if (typeof e == 'undefined') e = window.event;
        if (typeof e.layerX == 'undefined') e.layerX = e.offsetX;
        if (typeof e.layerY == 'undefined') e.layerY = e.offsetY;
        return e;
    }
};